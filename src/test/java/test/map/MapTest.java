package test.map;

import map.Map;
import org.junit.jupiter.api.Test;
import terrain.Air;
import terrain.TerrainElement;
import terrain.Wood;
import static org.assertj.core.api.Assertions.*;

public class MapTest {
  @Test
  void mapCreationTest() {
    Map actual = new Map("carte.txt");

    TerrainElement[][] expected = createTerrainElementExpected();

    assertThat(actual.getMap()).isEqualTo(expected);
  }

  @Test
  void mapCreationThrowsIllegalArgumentException() {
    assertThatThrownBy(() -> new Map("notfound")).isInstanceOf(IllegalArgumentException.class);
  }

  private TerrainElement[][] createTerrainElementExpected() {
    TerrainElement[][] expected = new TerrainElement[20][20];
    for (int y = 0; y < 20; y++) {
      for (int x = 0; x < 20; x++) {
        if (x == 0 || x == 19 || y == 0 || y == 19) {
          expected[x][y] = new Wood();
        } else expected[x][y] = new Air();
      }
    }
    return expected;
  }
}
