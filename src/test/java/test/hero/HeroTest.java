package test.hero;

import standard.Vector2D;
import hero.Hero;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

class HeroTest {
  private final Hero hero = new Hero();

  @Test
  void heroMoveUpTest() {
    Vector2D vecUp = new Vector2D(0, 1);
    hero.move(vecUp);

    Hero expected = new Hero(vecUp);

    assertThat(hero).isEqualTo(expected);
  }

  @Test
  void heroMoveDownTest() {
    Vector2D vecDown = new Vector2D(0, -1);
    hero.move(vecDown);

    Hero expected = new Hero(vecDown);

    assertThat(hero).isEqualTo(expected);
  }

  @Test
  void heroMoveLeftTest() {
    Vector2D vecLeft = new Vector2D(-1, 0);
    hero.move(vecLeft);

    Hero expected = new Hero(vecLeft);

    assertThat(hero).isEqualTo(expected);
  }

  @Test
  void heroMoveRightTest() {
    Vector2D vecRight = new Vector2D(1, 0);
    hero.move(vecRight);

    Hero expected = new Hero(vecRight);

    assertThat(hero).isEqualTo(expected);
  }
}
