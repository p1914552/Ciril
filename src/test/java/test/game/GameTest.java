package test.game;

import game.Game;
import map.Map;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import standard.Vector2D;

import static org.assertj.core.api.Assertions.*;

class GameTest {
  @Test
  void game1Test() {
    Game game = new Game(new Map("carte.txt"));
    game.startGame("game1.txt");

    Vector2D expected = new Vector2D(9, 2);

    assertThat(game.getHero().getVec()).isEqualTo(expected);
  }

  @Test
  void gameStartFileNotFoundThrowsIllegalArgumentException() {
    Map map = Mockito.mock(Map.class);

    assertThatThrownBy(() -> new Game(map).startGame("notFound"))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
