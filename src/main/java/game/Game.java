package game;

import hero.Hero;
import java.io.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import map.Map;
import standard.Vector2D;

/** Class that is used to start the game */
@Getter
@RequiredArgsConstructor
public class Game {
  private Hero hero;
  private final Map map;

  /**
   * Starts a game
   *
   * @param file, string : a file located in the resource folder
   */
  public void startGame(String file) {
    InputStream is = getClass().getClassLoader().getResourceAsStream(file);
    if (is == null) throw new IllegalArgumentException("Please provide a file to start a game");
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    try {
      spawnHero(reader);
      executeHeroActions(reader);
    } catch (IOException e) {
      throw new IllegalStateException("Something wrong happened while parsing the file " + file, e);
    }
  }

  private void executeHeroActions(BufferedReader reader) throws IOException {
    reader.readLine().chars().mapToObj((i -> (char) i)).forEach(this::moveHero);
  }

  private void moveHero(Character character) {
    Vector2D direction = getDirectionFromChar(character);
    if (checkDirection(direction)) hero.move(direction);
  }

  private boolean checkDirection(Vector2D direction) {
    int x = hero.getVec().getX() + direction.getX();
    int y = hero.getVec().getY() + direction.getY();
    if (x < 0 || x >= map.getXLength() || y < 0 || y >= map.getYLength()) return false;
    return !map.containsSolid(x, y);
  }

  private Vector2D getDirectionFromChar(Character character) {
    return switch (character) {
      case 'S' -> new Vector2D(0, 1);
      case 'N' -> new Vector2D(0, -1);
      case 'E' -> new Vector2D(1, 0);
      case 'O' -> new Vector2D(-1, 0);
      default -> new Vector2D();
    };
  }

  private void spawnHero(BufferedReader reader) throws IOException {
    String line = reader.readLine();
    hero = new Hero(new Vector2D(Vector2D.StringToVec(line)));
  }
}
