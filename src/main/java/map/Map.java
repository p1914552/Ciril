package map;

import lombok.Getter;
import terrain.Air;
import terrain.TerrainElement;
import terrain.Wood;

import java.io.*;
import java.util.*;

/** Class that represents the Map where the Hero will be */
@Getter
public class Map {
  private final TerrainElement[][] map;

  private int xLength;
  private int yLength;

  /**
   * Constructor that takes in a file name and fills the map.
   *
   * @param file, String
   */
  public Map(String file) {
    InputStream is = getClass().getClassLoader().getResourceAsStream(file);
    if (is == null) throw new IllegalArgumentException("Please provide a file to create a map");
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));
      map = getMapFromReader(reader);
      reader.close();
    } catch (IOException e) {
      throw new IllegalStateException("Something went wrong while parsing the file : " + file, e);
    }
  }

  private TerrainElement[][] getMapFromReader(BufferedReader reader) throws IOException {
    List<TerrainElement[]> terrainElements = new ArrayList<>();
    String line = reader.readLine();
    while (line != null) {
      terrainElements.add(stringToTerrainElementArray(line));
      line = reader.readLine();
    }

    xLength = maxSizeInArray(terrainElements);
    yLength = terrainElements.size();
    TerrainElement[][] ret = new TerrainElement[yLength][xLength];

    for (int i = 0; i < terrainElements.size(); i++) {
      ret[i] = terrainElements.get(i);
    }
    return ret;
  }

  private int maxSizeInArray(List<TerrainElement[]> terrainElements) {
    return terrainElements.stream()
        .max(Comparator.comparingInt(line -> line.length))
        .orElseThrow(() -> new IllegalStateException("Cannot find a max in the arrays"))
        .length;
  }

  private TerrainElement[] stringToTerrainElementArray(String str) {
    int size = str.length();
    TerrainElement[] terrainElements = new TerrainElement[size];
    for (int i = 0; i < size; i++) {
      char character = str.charAt(i);
      if (character == '#') terrainElements[i] = new Wood();
      else terrainElements[i] = new Air();
    }
    return terrainElements;
  }

  /**
   * Checks if the emplacement (x,y) of the map is solid or not
   *
   * @param x : integer
   * @param y : integer
   * @return True if the element of position x,y is solid. False otherwise.
   */
  public boolean containsSolid(int x, int y) {
    return map[y][x].isSolid();
  }

  /** Made for fast debugging, kept to show Ciril functional coding */
  public void printMap() {
    Arrays.stream(map).forEach((this::printLine));
  }

  private void printLine(TerrainElement[] terrainElements) {
    Arrays.stream(terrainElements)
        .forEach(
            element -> {
              if (element.isSolid()) System.out.print("X");
              else System.out.print(" ");
            });
    System.out.println();
  }
}
