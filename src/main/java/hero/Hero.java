package hero;

import standard.Vector2D;
import lombok.AllArgsConstructor;
import lombok.Data;

/** Class that represents the hero of our game */
@Data
@AllArgsConstructor
public class Hero {
  private Vector2D vec;

  /** Default constructor, will set the Vector2D of the Hero to (0,0) */
  public Hero() {
    vec = new Vector2D();
  }

  /**
   * Moves the character towards the direction of a Vector
   *
   * @param vecDirect a Vector2D used to give a direction
   */
  public void move(Vector2D vecDirect) {
    vec = Vector2D.Add(vec, vecDirect);
  }
}
