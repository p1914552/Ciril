package terrain;

import lombok.EqualsAndHashCode;

/** Wood, a terrain element that is not cross-able */
@EqualsAndHashCode
public class Wood implements TerrainElement {
  @Override
  public boolean isSolid() {
    return true;
  }
}
