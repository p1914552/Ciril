package terrain;

/** Interface for element in the terrain */
public interface TerrainElement {
  /**
   * Checks if the element is solid or not
   *
   * @return true if solid, false if not
   */
  boolean isSolid();
}
