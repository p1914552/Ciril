package terrain;

import lombok.EqualsAndHashCode;

/** Air, a terrain element that is cross-able */
@EqualsAndHashCode
public class Air implements TerrainElement {

  @Override
  public boolean isSolid() {
    return false;
  }
}
