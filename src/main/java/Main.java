import game.Game;
import map.Map;

/**
 * Main class of the program, used to create the Game and play it
 */
public class Main {
    public static void main(String[] args)
    {
        Game game = new Game(new Map("carte.txt"));
        game.startGame("game2.txt");
        System.out.print(game.getHero().getVec().getX() + "," + game.getHero().getVec().getY());
    }
}