package standard;

import lombok.AllArgsConstructor;
import lombok.Data;

/** Util class, A simple Vector of 2 dimension that contains an x and y variable; */
@Data
@AllArgsConstructor
public class Vector2D {
  private int x;
  private int y;

  /** base constructor, sets X and Y to 0 */
  public Vector2D() {
    x = 0;
    y = 0;
  }

  /**
   * Copy constructor
   *
   * @param vec a Vector2D
   */
  public Vector2D(Vector2D vec) {
    x = vec.x;
    y = vec.y;
  }

  /**
   * add 2 vector
   *
   * @param vec1 a Vector2D
   * @param vec2 a Vector2D
   * @return the result of the addition between the 2 vectors
   */
  public static Vector2D Add(Vector2D vec1, Vector2D vec2) {
    return new Vector2D(vec1.getX() + vec2.getX(), vec1.getY() + vec2.getY());
  }

  /**
   * Creates a vector from a "X,Y" string
   *
   * @param str string with a form like "X,Y"
   * @return a Vector2D that's been created from the string str
   */
  public static Vector2D StringToVec(String str) {
    String[] coordinates = str.split(",");
    return new Vector2D(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]));
  }
}
