# CirilGroup

## How to launch the game

### Compile and .jar

This project has been compiled with maven's compiler.

To compile and create the .jar, just do
`` mvn site ``

- .jar will be located in
` ./target/Ciril-1.0.jar `

- Documentation will be located in ``./target/site/index.html`` 

## Problems

In the given excercise, the 2nd test :
`4,3
OONOOOSSO
`

Is supposed to place the hero at (7,5), but in reality it places him at (2,4)
